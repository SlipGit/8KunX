# ![8](https://raw.githubusercontent.com/SlippingGitty/8chanX-for-8kun/2-0_pure/images/logo.png)kunX 2021.01.18.04  [Click here to install](https://github.com/SlippingGitty/8chanX-for-8kun/raw/2-0_pure/8kunX.user.js)

Current New Features: 
 * Yandex reverse image support added 
 * Removed the new 8Kun legal garbage [forked from here](https://github.com/4FK/8kun-disclaimer-hider)
 * Rided of the bitcoin ad
 * Merged marktaiwan's [8kun formating tools](https://github.com/marktaiwan/8kun-Formatting-Tools)
 * Replaced logo and favicon with a oldish-new one

***

This userscript adds various features and options like:
 * Gallery
 * Filters
 * Reverse image search
 * Flag preview
 * Mascots
 * Notifications
 * Relative post dates
 * Post and image counts in the menu
 * Many other poorly written hacks
 
Key bindings
Key     | Function
----    | ----
C       | Navigates to the catalog
E       | Expands/shrinks all images
G       | Toggles the gallery
Q       | Opens the quick reply
R       | Update thread/reload page
Esc     | Closes the quick reply, gallery, or expanded gallery image

This is a userscript, you will need an addon to run it properly:

Browser|Addon
----   |----
Firefox|[Violentmonkey](https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/)
Chrome |[Violentmonkey](https://chrome.google.com/webstore/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag)
